EESchema Schematic File Version 2
LIBS:func
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:coursework-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Схема электрическая\\nфункциональная"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 "Одишария Г.М."
Comment3 "Хохлов С.А."
Comment4 "Хохлов С.А."
$EndDescr
$Comp
L flash fl?
U 1 1 584C61EB
P 6810 5490
F 0 "fl?" H 6760 5890 60  0001 C CIN
F 1 "flash" H 6560 5890 60  0001 C CIN
F 2 "" H 6810 5490 60  0001 C CNN
F 3 "" H 6810 5490 60  0001 C CNN
	1    6810 5490
	1    0    0    -1  
$EndComp
$Comp
L ddr2 D?
U 1 1 584C61FF
P 8200 5480
F 0 "D?" H 7800 5710 60  0001 C CNN
F 1 "ddr2" H 7760 5640 60  0001 C CNN
F 2 "" H 8200 5480 60  0001 C CNN
F 3 "" H 8200 5480 60  0001 C CNN
	1    8200 5480
	1    0    0    -1  
$EndComp
Wire Wire Line
	6230 5410 6280 5410
Wire Wire Line
	6210 5490 6280 5490
Wire Wire Line
	6190 5570 6280 5570
$Comp
L интерфейсDSP D?
U 1 1 584C6531
P 9670 3120
F 0 "D?" H 9450 3580 60  0001 C CNN
F 1 "интерфейсDSP" H 9070 3600 60  0001 C CNN
F 2 "" H 9670 3120 60  0001 C CNN
F 3 "" H 9670 3120 60  0001 C CNN
	1    9670 3120
	1    0    0    -1  
$EndComp
Wire Wire Line
	8620 3010 9040 3010
Wire Wire Line
	8620 3090 9040 3090
Wire Wire Line
	8620 3170 9040 3170
Wire Wire Line
	8620 3250 9040 3250
Text Notes 9050 3010 0    39   Italic 0
SCLK
Text Notes 9050 3090 0    39   Italic 0
MISO
Text Notes 9050 3170 0    39   Italic 0
MOSI
Text Notes 9120 3250 0    39   Italic 0
~SS
$Comp
L интерфейсПК D?
U 1 1 584C672B
P 2650 3350
F 0 "D?" H 2430 3810 60  0001 C CNN
F 1 "интерфейсПК" H 2050 3830 60  0001 C CIN
F 2 "" H 2650 3350 60  0001 C CNN
F 3 "" H 2650 3350 60  0001 C CNN
	1    2650 3350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3700 3250 3280 3250
Wire Wire Line
	3700 3330 3280 3330
Wire Wire Line
	3700 3490 3280 3490
Text Notes 3200 3250 2    39   Italic 0
DP
Text Notes 3200 3330 2    39   Italic 0
DM
Text Notes 3270 3490 2    39   Italic 0
VBUS
Wire Wire Line
	6230 5410 6230 5240
Wire Wire Line
	6230 5240 7250 5240
Wire Wire Line
	7250 5240 7250 4990
Wire Wire Line
	7170 4990 7170 5180
Wire Wire Line
	7170 5180 6210 5180
Wire Wire Line
	6210 5180 6210 5490
Wire Wire Line
	6190 5570 6190 5120
Wire Wire Line
	6190 5120 7090 5120
Wire Wire Line
	7090 5120 7090 4990
$Comp
L AM3358 МК?
U 1 1 584CB2C6
P 3610 860
F 0 "МК?" H 4590 1030 60  0001 C CIN
F 1 "AM3358" H 4860 1030 60  0001 C CIN
F 2 "" H 5190 960 60  0001 C CNN
F 3 "" H 5190 960 60  0001 C CNN
	1    3610 860 
	1    0    0    -1  
$EndComp
Wire Wire Line
	7680 5420 7620 5420
Wire Wire Line
	7620 5420 7620 4990
Wire Wire Line
	7550 4990 7550 5540
Wire Wire Line
	7550 5540 7680 5540
Wire Notes Line
	3820 4870 3820 250 
Wire Notes Line
	3820 250  8500 250 
Wire Notes Line
	8500 250  8500 4870
Wire Notes Line
	8500 4870 3820 4870
Text Notes 4510 410  0    78   Italic 0
Модуль управления генератора-анализатора сигналов
Text Label 6240 5570 0    24   Italic 0
MMC_DAT
Text Label 6240 5490 0    24   Italic 0
MMC_CMD
Text Label 6240 5410 0    24   Italic 0
MMC_CLK
Text Label 7630 5410 0    24   Italic 0
DDR_CMD
Text Label 7630 5530 0    24   Italic 0
DDR_DAT
$EndSCHEMATC
